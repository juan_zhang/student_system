from flask import Flask,render_template,jsonify,request
from flask_sqlalchemy import SQLAlchemy
'''
pip install flask
pip install flask_sqlalchemy

'''


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:YOUR_PWD@127.0.0.1:3306/StudentDB'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

class Student(db.Model):
    __tablename__ = 'student_info' # this will create table 'student_info' in database
    id = db.Column(db.Integer,primary_key=True)
    name = db.Column(db.String(64))
    age = db.Column(db.String(512))
    gender = db.Column(db.String(512))
    email = db.Column(db.String(512))
    def __repr__(self):
        return "id: %d, name: %s, age: %s" % (self.id, self.name, self.age,)

def  add(record):

    db.session.add(record)
    db.session.commit()


@app.route('/')
def index():
    return render_template('index.html')


# ==================create student===============
@app.route('/',methods=['GET','POST'])
def reg_user(): # form表单用post提交过来的
    # 拿到学生各种信息，开始存数据
    if request.method=='POST' :
        name = request.form.get('username')
        age = request.form.get("age",'')
        gender = request.form.get("gender",'')
        email = request.form.get('email','')# set to null if can not find
        print(name,age,gender,email)
        # Create the instance of the 'student_info' table
        record = Student(
            name=name,
            age=age,
            gender=gender,
            email=email
        )
        add(record)
        return "Success !"


# ==================search student===============
@app.route('/search/',methods=['GET','POST'])
def search():

    if request.method == 'GET':

        name = request.args.get("stuname")# http://127.0.0.1:5067/search/?stuname=vera

        print(type(name),name)
    res_list = Student.query.filter(Student.name.startswith(name)).all()

    return render_template('result.html', list = res_list)

#======================update student info===========
@app.route('/update/',methods=['GET','POST'])
def update():
    if request.method == 'POST':

        id = request.form.get('id','')
        name = request.form.get('name','')
        age = request.form.get('age','')
        gender = request.form.get('gender','')
        email = request.form.get('email','')

        print(id,name,age,gender,email)
        # query by student id
        newinfo = Student(
            name=name,
            age=age,
            gender=gender,
            email=email
        )
        student = Student.query.get(id)
        student.name= name
        student.age = age
        student.gender = gender
        student.email = email
        db.session.add(student)
        db.session.commit()

    return jsonify('ok')

# =================return all the record in db============
@app.route('/getinfo/',methods=['GET','POST'])
def getinfo():

    if request.method == 'GET':

        res_list = Student.query.all() # 这个列表装的是student对象
        print(res_list)

    return render_template('showall.html', list=res_list) # sent student list back to fontend

#================Delete student record===============
@app.route('/delete/',methods=['GET','POST'])
def delete():
    res = Student()
    if request.method == 'POST':
        id = request.form.get('id','')
        print(id)
        student =  Student.query.get(id)
        db.session.delete(student)
        db.session.commit()
        # 从数据库中删除这个学号的人 delete this student from database

    return 'delete'



if __name__ == '__main__':
    # db.create_all() # run this only once for the first time 跑一次自动建表


    app.run(port='5071')

