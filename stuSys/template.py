from flask import Flask, render_template,url_for

# 在flask中默认添加了 template_folder=templates
app = Flask(__name__)

# 创建一个模板
'''
    模板的步骤
    1. 引用模板render_template
    2. 在路由中使用模板
    3. 在最后的return中返回模板（本质就是模板转换成字符串）
    4. 在templates文件夹中创建模板
    5. 将模板的路径添加render_template
'''


@app.route('/')
def index():
    # render_template 就是将模板转换成字符串
    return render_template('index.html')


# 传入字典
@app.route('/demo4')
def demo4():
    # 定义学生信息
    # id 姓名 年龄 性别 1 男 0 女 -1 保密
    list_person = [
        {
            'id': 100,
            'name': '张三',
            'age': 18,
            'sex': 1
        },
        {
            'id': 101,
            'name': '张大麻子',
            'age': 38,
            'sex': 0
        },
        {
            'id': 102,
            'name': '小鹿',
            'age': 28,
            'sex': -1
        },
        {
            'id': 103,
            'name': '小强',
            'age': 48,
            'sex': -1
        }
    ]
    return render_template('template_5.html', list=list_person)

# def filter_sex(data):
#     # 如果是1 就是男  0 就是女  其他值就是保密
#     if data==1:
#         return '男'
#     elif data==0:
#         return '女'
#     else:
#         return '保密'
#
# app.add_template_filter(filter_sex, 'sex')


# 第二种
# 装饰器的参数定义了该过滤器的名称

if __name__ == '__main__':
    app.run(debug=True, port=8899)

